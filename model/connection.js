const mongoose = require('mongoose');

const dbURI = `mongodb://rassel333:vld777@cluster0-shard-00-00-unkwf.mongodb.net:27017,cluster0-shard-00-01-unkwf.mongodb.net:27017,cluster0-shard-00-02-unkwf.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true`
mongoose.connect(dbURI, { dbName: 'FootwearCatalog' });

mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

mongoose.set('debug', true);