const Supplier = require('../schemas/supplier').Supplier;
const mongoose = require('mongoose');

function getAllsuppliers(callback) {
    Supplier.find(null, callback)
}

function getReliableSuppliers(callback) {
    Supplier.find({ isReliable: true }, callback)
}