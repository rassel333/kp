const Footwear = require('../schemas/footwear').Footwear;
const mongoose = require('mongoose');

function buildFootwearFilters(filters) {
    const dataFilters = {};
    Object.keys(filters).forEach(filterKey =>{
        if (Array.isArray(filters[filterKey])) {
            dataFilters[filterKey] = { $in: filters[filterKey] };
        } else {
            dataFilters[filterKey] = filters[filterKey];
        }
    })
    return dataFilters;
}

function getAllFootwear(callback) {
    Footwear.find(null, callback);
}

function createFootwear(data, callback) {
    Footwear.create({
        _id: new mongoose.Types.ObjectId(),
        ...data
    }, callback);
}

function updateFootwearById(id, updateData, callback) {
    Footwear.findByIdAndUpdate(id,
        updateData,
        callback,
        {new: true}
    );
}

function getFootwearById(id, callback) {
    Footwear.findById(id, callback);
}

function getFilteredFootwear(filters, callback) {
    let dataFilters = {};
    Object.keys(filters)
        .filter(key => key !== 'priceMin' && key !== 'priceMax' && filters[key])
        .forEach(key => {
            dataFilters[key] = filters[key]
        });
    dataFilters = buildFootwearFilters(dataFilters);
    if (filters.priceMin) {
        dataFilters.price = {};
        dataFilters.price['$gte'] = filters.priceMin;
    }
    if (filters.priceMax) {
        dataFilters.price = dataFilters.price || {};
        dataFilters.price['$lte'] = filters.priceMax;
    }
    if (dataFilters.brand) {
        delete dataFilters.brand;
        dataFilters['brand.name'] = filters.brand;
    }
    console.log('FILTERS_LOG ===> ', dataFilters);
    return Footwear.find(dataFilters, callback);
}

function removeFootwear(id, callback) {
    Footwear.findByIdAndRemove(id, callback);
}

function getLastAddedFootwear(limit = 1, callback) {
    Footwear.find(null, null, { limit }, callback);
}

function getAllBrands(callback) {
    Footwear.find(null, 'brand', callback);
}

function getBrandsByFootwearType(type, callback) {
    Footwear.find({type}, 'brand', callback);
}

module.exports = {
    getAllFootwear,
    createFootwear,
    updateFootwearById,
    getFilteredFootwear,
    getLastAddedFootwear,
    removeFootwear,
    getAllBrands,
    getBrandsByFootwearType,
    getFootwearById
}