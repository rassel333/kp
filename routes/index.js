const express = require('express');
const router = express.Router();
const async = require('async');
const footwearController = require('../controllers/footwearController');

/* GET home page. */
router.get('/', (req, res) => {
    const pageData = {};
    return async.waterfall([
            next => footwearController.getLastAddedFootwear(6, next),
            (lastAddedFootwear, next) => {
                pageData.bannerShoes = lastAddedFootwear.slice(0, 3);
                pageData.lastAddeFootwear = lastAddedFootwear;
                return next();
            }
        ],
        error => {
            if (error) res.render('error', error)
            return res.render('index', pageData);
        })
});

router.get('/shop/:id', (req, res) => {
    const {id} = req.params;
    const pageData = {};
    return async.waterfall([
        next => footwearController.getFootwearById(id, next),
        (footwear, next) => {
            pageData.footwear = footwear;
            console.log(footwear)
            return footwearController.getFilteredFootwear({ brand: footwear.brand.name }, next)
        },
        (recommendedFootwear = [], next) => {
        console.log('FOOTWEAR_LOG ===>', recommendedFootwear)
            pageData.recommendedProducts = recommendedFootwear.filter(fw => {
                return JSON.stringify(fw._id) !== JSON.stringify(pageData.footwear._id);
            });
            return next()
        }
    ], (error) => {
        if (error) res.render('error', error)
        return res.render('productDetails', pageData);
    })
})

module.exports = router;
