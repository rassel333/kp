const express = require('express');
const async = require('async');
const router = express.Router();
const FootwearController = require('../controllers/footwearController');

/* API methods */
router.post('/', (req, res) => {
    const {body: footwearData} = req;
    FootwearController.createFootwear(footwearData, (error, footwear) => {
        res.send(footwear);
    });
});

router.patch('/:id', (req, res) => {
    const {id} = req.params;
    FootwearController.updateFootwearById(id, (error, footwear) => {
        res.send(footwear);
    });
});


router.get('/', (req, res) => {
    const {size, brand, country, priceMax, priceMin, gender} = req.query;
    FootwearController.getFilteredFootwear({ size, brand, priceMax, priceMin, country, gender }, (error, footwear) => {
        res.send(footwear);
    });
});

router.delete('/:id', (req, res) => {
    const {id} = req.params;
    FootwearController.removeFootwear(id, (error) => {
        res.send(id)
    });
});

router.get('/brands')


module.exports = router;
