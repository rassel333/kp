const Schema = require('mongoose').Schema;
const model = require('mongoose').model;

const supplierSchema = Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    address: String,
    isReliable: Boolean
});

const Supplier = model('Supplier', supplierSchema);

module.exports = {
    Supplier
};