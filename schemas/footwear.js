const Schema = require('mongoose').Schema;
const model = require('mongoose').model;

const footwearSchema = Schema({
    _id: Schema.Types.ObjectId,
    type: String,
    title: String,
    size: [Number],
    gender: String,
    country: String,
    description: String,
    brand: { name: String, imageUrl: String, description: String },
    suppliers: [{ type: Schema.Types.ObjectId, ref: 'Supplier' }],
    price: Number,
    imageUrl: String,
    detailImages: [String]
});

const Footwear = model('Footwear', footwearSchema);

module.exports = {
    Footwear
};